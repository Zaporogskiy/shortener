FROM openjdk:11
MAINTAINER zaporozhskiy
ADD target/url-shortener.jar url-shortener.jar
ENTRYPOINT ["java", "-jar","url-shortener.jar"]
EXPOSE 8080