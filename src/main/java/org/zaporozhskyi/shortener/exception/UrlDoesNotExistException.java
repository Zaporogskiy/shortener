package org.zaporozhskyi.shortener.exception;

public class UrlDoesNotExistException extends RuntimeException {

    public UrlDoesNotExistException(String message) {
        super(message);
    }
}
