package org.zaporozhskyi.shortener.helper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.zaporozhskyi.shortener.exception.UrlDoesNotExistException;
import org.zaporozhskyi.shortener.exception.UrlShortenerException;

@Slf4j
@ControllerAdvice
public class ExceptionHelper {

    @ExceptionHandler(value = { UrlShortenerException.class })
    public ResponseEntity<Object> handleUrlShortenerException(UrlShortenerException ex) {
        log.info("Exception {} was handled.", ex.toString());
        return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { UrlDoesNotExistException.class })
    public ResponseEntity<Object> handleUrlDoesNotExistException(UrlDoesNotExistException ex) {
        log.info("Exception {} was handled.", ex.toString());
        return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { RuntimeException.class })
    public ResponseEntity<Object> handleRuntimeException(RuntimeException ex) {
        log.info("Unexpected Exception {} was handled.", ex.toString());
        return new ResponseEntity<Object>("Request cant be handled.", HttpStatus.BAD_REQUEST);
    }
}