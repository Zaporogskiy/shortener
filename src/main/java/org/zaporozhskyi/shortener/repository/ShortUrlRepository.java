package org.zaporozhskyi.shortener.repository;


import org.springframework.data.repository.Repository;
import org.zaporozhskyi.shortener.entity.ShortUrl;

public interface ShortUrlRepository extends Repository<ShortUrl, Integer> {

    ShortUrl save(ShortUrl entity);

    ShortUrl findByShortUrl(String shortUrl);

}
