package org.zaporozhskyi.shortener.builder;

import org.springframework.stereotype.Component;
import org.zaporozhskyi.shortener.response.GenerateResponse;

@Component
public class GenerateResponseBuilder {

    public GenerateResponse build(String shortUrl) {
        return GenerateResponse.builder().urlShortener(shortUrl).build();
    }
}