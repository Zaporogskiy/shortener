package org.zaporozhskyi.shortener.validator;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.HttpURLConnection;

@Component
@RequiredArgsConstructor
public class UrlExistValidator {

    @Autowired
    private final RestTemplate restTemplate;

    public boolean validate(String url) {
        // todo unable to find valid certification error -> http://presight.ai
        if (StringUtils.isBlank(url)) {
            return false;
        }
        ResponseEntity<Void> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, Void.class);
        return response.getStatusCode().value() == HttpURLConnection.HTTP_OK;
    }
}
