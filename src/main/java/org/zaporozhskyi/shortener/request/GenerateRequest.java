package org.zaporozhskyi.shortener.request;

import lombok.Data;

@Data
public class GenerateRequest {
    private String url;
}
