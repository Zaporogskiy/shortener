package org.zaporozhskyi.shortener.request;

import lombok.Data;

@Data
public class GoToRequest {

    private final String urlShortener;
}
