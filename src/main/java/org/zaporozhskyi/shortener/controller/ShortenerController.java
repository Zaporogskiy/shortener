package org.zaporozhskyi.shortener.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zaporozhskyi.shortener.entity.ShortUrl;
import org.zaporozhskyi.shortener.response.GenerateResponse;
import org.zaporozhskyi.shortener.request.GenerateRequest;
import org.zaporozhskyi.shortener.request.GoToRequest;
import org.zaporozhskyi.shortener.service.UrlShortenerService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequiredArgsConstructor
public class ShortenerController {

    private final UrlShortenerService urlShortenerService;

    @Operation(summary = "Return generated short url.",
            tags = {
                    "Url Shortener Generator"
            },
            description = "Return generated value for input url. Current value are hardcoded.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful response", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = GenerateResponse.class))
            }),
            @ApiResponse(responseCode = "400", description = "BadRequest", content = @Content),
    })
    @PostMapping("/generate")
    public GenerateResponse generate(@RequestBody GenerateRequest request) {
        return urlShortenerService.generate(request.getUrl());
    }

    @Operation(summary = "Redirect to the url",
            tags = {
                    "Url Redirecting"
            },
            description = "Input short URL. Find original URL by short URL. Redirect to original URL.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful response"),
            @ApiResponse(responseCode = "400", description = "BadRequest"),
    })
    @RequestMapping(value = "/goto/url", method = RequestMethod.GET)
    public void method(HttpServletResponse httpServletResponse, @ParameterObject GoToRequest request) {
        ShortUrl response = urlShortenerService.getUrlByShortName(request.getUrlShortener());
        httpServletResponse.setHeader("Location", response.getUrl());
        httpServletResponse.setStatus(302);
    }
}