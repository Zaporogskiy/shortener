package org.zaporozhskyi.shortener.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.zaporozhskyi.shortener.builder.GenerateResponseBuilder;
import org.zaporozhskyi.shortener.entity.ShortUrl;
import org.zaporozhskyi.shortener.exception.UrlDoesNotExistException;
import org.zaporozhskyi.shortener.exception.UrlShortenerException;
import org.zaporozhskyi.shortener.repository.ShortUrlRepository;
import org.zaporozhskyi.shortener.response.GenerateResponse;
import org.zaporozhskyi.shortener.validator.UrlExistValidator;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UrlShortenerService {

    @Value("${application.url-shortener.target-url}")
    public String targetUrl;
    @Value("${application.url-shortener.target-short-url}")
    public String shortUrl;
    private final ShortUrlRepository shortUrlRepository;
    private final UrlExistValidator urlExistValidator;
    private final GenerateResponseBuilder generateResponseBuilder;

    public GenerateResponse generate(String url) {
        String validUrl = Optional.of(url)
                .filter(urlExistValidator::validate)
                .filter(u -> u.equalsIgnoreCase(this.targetUrl))
                .orElseThrow(() -> new UrlShortenerException("Url " + url + " is not valid."));

        ShortUrl entity = shortUrlRepository.save(ShortUrl.builder().shortUrl(shortUrl).url(validUrl).build());
        return generateResponseBuilder.build(entity.getShortUrl());
    }

    public ShortUrl getUrlByShortName(String shortUrl) {
        ShortUrl byShortUrl = shortUrlRepository.findByShortUrl(shortUrl);
        return Optional.ofNullable(byShortUrl)
                .orElseThrow(() -> new UrlDoesNotExistException("Can't find url with short name  " + shortUrl));
    }
}
