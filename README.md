# Shortener

##Precondition:
1. Install/Run docker. 
2. terminal -> mvn clean install
3. terminal -> docker build -t url-shortener .
4. Go to game/infrastructure folder: terminal -> docker-compose up

##Use case: 
1. Make POST request  http://localhost:8080/generate
body:
{
    "url": "https://google.com.ua/"
}

2. Response should be: 
{
    "urlShortener": "abcde"
}

3. Make GET request to http://localhost:8080/goto/url?urlShortener=abcde
4. Response should redirect to https://www.google.com.ua/ according to step 1. 

##Important
There are a few limits of the implementations: 
1. An application uses embedded Datastore for testing reason. That's why data are saved in memmory. So each relaunched firstly you should generate short and then call goTo endpoint. 
If you call GoTo endpoint firstly - you can check exception handling. 

2. Keep in mind, that application use environment variables. They should be initialized inside docker compose file. 
Default values are: 
TARGET_URL=https://google.com.ua/
TARGET_SHORT_URL=abcde

##Documentation:
Go to http://localhost:8080/swagger-ui.html